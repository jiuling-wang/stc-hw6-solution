      program bar
      integer i,j,n,one
      parameter (n=3)
      parameter (one=1)
      integer ipiv(n),info
      real*8 a(n,n)
      real*8 b(n)

!  A(0,0)=6.;  A(0,1)=-2.;  A(0,2)=2.;
!  A(1,0)=12.; A(1,1)=-8.;  A(1,2)=6.;
!  A(2,0)=3.;  A(2,1)=-13.; A(2,2)=3.;

      a(1,1)=6.
      a(1,2)=-2.
      a(1,3)=2.
      a(2,1)=12.
      a(2,2)=-8.
      a(2,3)=6.
      a(3,1)=3.
      a(3,2)=-13.
      a(3,3)=3.    
      b(1)=16.
      b(2)=26.
      b(3)=-19.
      call dgesv(n,one,a,n,ipiv,b,n,info) 
      do i=1,n
      write(*,'(10X,F8.5)') b(i)
      enddo
      stop
      end

