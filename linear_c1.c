#include <stdlib.h>
#include <stdio.h>

// address polynomials for C arrays passed by reference to Fortran Routine
 
#define A(i,j) a[(i) + (j)*n]
#define A_original(i,j) a_original[(i) + (j)*n]
#define A_inverse(i,j) a_inverse[(i) + (j)*n]
#define Unity(i,j) unity[(i) + (j)*n]


int main()
{
  int i,j,n=5,one=1,info;
  double alpha=1.0, beta=0.0;
  double a[n*n], a_inverse[n*n], a_original[n*n], unity[n*n];
//  double A[n][n], A_inverse[n][n], A_original[n][n], Unity[n][n];
  int ipiv[n];

//  for(i=0;i<n;++i)
//     {
//      for(j=0;j<n;++j)
//        {
//          A(i,j) = 1.0/(i+j+1);
//          a[i+j*n] = ((double) i + (double) j+1.0);
//          A_inverse(i,j) = A(i,j);
//          a_inverse[i+j*n] = a[i+j*n];
//          if (i=j){
//             Unity(i,j) = 1.0;
//             unity[i+j*n] = 1.0;
//             }
//          else {
//             Unity(i,j) = 0.0;
//             unity[i+j*n] = 0.0;
//             }
//        }
//      }
  A(0,0)=1.0/1.0;  A(0,1)=1.0/2.0;  A(0,2)=1.0/3.0; A(0,3)=1.0/4.0; A(0,4)=1.0/5.0;
  A(1,0)=1.0/2.0;  A(1,1)=1.0/3.0;  A(1,2)=1.0/4.0; A(1,3)=1.0/5.0; A(1,4)=1.0/6.0;
  A(2,0)=1.0/3.0;  A(2,1)=1.0/4.0;  A(2,2)=1.0/5.0; A(2,3)=1.0/6.0; A(2,4)=1.0/7.0;
  A(3,0)=1.0/4.0;  A(3,1)=1.0/5.0;  A(3,2)=1.0/6.0; A(3,3)=1.0/7.0; A(3,4)=1.0/8.0;
  A(4,0)=1.0/5.0;  A(4,1)=1.0/6.0;  A(4,2)=1.0/7.0; A(4,3)=1.0/8.0; A(4,4)=1.0/9.0;

  A_original(0,0)=1.0/1.0;  A_original(0,1)=1.0/2.0;  A_original(0,2)=1.0/3.0; A_original(0,3)=1.0/4.0; A_original(0,4)=1.0/5.0;
  A_original(1,0)=1.0/2.0;  A_original(1,1)=1.0/3.0;  A_original(1,2)=1.0/4.0; A_original(1,3)=1.0/5.0; A_original(1,4)=1.0/6.0;
  A_original(2,0)=1.0/3.0;  A_original(2,1)=1.0/4.0;  A_original(2,2)=1.0/5.0; A_original(2,3)=1.0/6.0; A_original(2,4)=1.0/7.0;
  A_original(3,0)=1.0/4.0;  A_original(3,1)=1.0/5.0;  A_original(3,2)=1.0/6.0; A_original(3,3)=1.0/7.0; A_original(3,4)=1.0/8.0;
  A_original(4,0)=1.0/5.0;  A_original(4,1)=1.0/6.0;  A_original(4,2)=1.0/7.0; A_original(4,3)=1.0/8.0; A_original(4,4)=1.0/9.0;

    Unity(0,0)=1.0; Unity(0,1)=0.0; Unity(0,2)=0.0; Unity(0,3)=0.0; Unity(0,4)=0.0;
    Unity(1,0)=0.0; Unity(1,1)=1.0; Unity(1,2)=0.0; Unity(1,3)=0.0; Unity(1,4)=0.0;
    Unity(2,0)=0.0; Unity(2,1)=0.0; Unity(2,2)=1.0; Unity(2,3)=0.0; Unity(2,4)=0.0;
    Unity(3,0)=0.0; Unity(3,1)=0.0; Unity(3,2)=0.0; Unity(3,3)=1.0; Unity(3,4)=0.0;
    Unity(3,0)=0.0; Unity(4,1)=0.0; Unity(4,2)=0.0; Unity(4,3)=0.0; Unity(4,4)=1.0;



// print original A
  printf("A\n");
  for(i=0;i<n;++i)
     {
      for(j=0;j<n;++j)
        {
          printf("%16.6E ",A(i,j));
//          printf("%8.5g ",a[i+j*n]);
        }
      printf("\n");
    }

// find A_inverse by calling dgesv

  dgesv_(&n,&n,a,&n,ipiv,unity,&n,&info);

  printf("A^-1\n");
  for(i=0;i<n;++i)
     {
      for(j=0;j<n;++j)
        {
          A_inverse(i,j) = Unity(i,j);
          printf("%16.6E ",A_inverse(i,j));
        }
      printf("\n");
    }

// verify rsults by call dgemm

  dgemm_("N","N", &n, &n, &n, &alpha, a_inverse, &n, a_original, &n, &beta, unity, &n);

  printf("A^-1 * A\n");
  for(i=0;i<n;++i)
     {
      for(j=0;j<n;++j)
        {
          printf("%16.6E ",Unity(i,j));
        }
      printf("\n");
    }

// verify rsults by call dgemm

  dgemm_("N","N", &n, &n, &n, &alpha, a_original, &n, a_inverse, &n, &beta, unity, &n);

  printf("A * A^-1\n");
  for(i=0;i<n;++i)
     {
      for(j=0;j<n;++j)
        {
          printf("%16.6E ",Unity(i,j));
        }
      printf("\n");
    }

  return(0);
}

