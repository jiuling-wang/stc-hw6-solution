      program linear
      integer i,j,n,one
      real*8 alpha, beta
      parameter (n=5)
      parameter (one=1)
      parameter (alpha = 1.d0)
      parameter (beta = 0.d0)
      integer ipiv(n),info
      real*8 a(n,n), a_original(n,n), a_inverse(n,n),unity(n,n)
      real*8 b(n)

!  A(0,0)=6.;  A(0,1)=-2.;  A(0,2)=2.;
!  A(1,0)=12.; A(1,1)=-8.;  A(1,2)=6.;
!  A(2,0)=3.;  A(2,1)=-13.; A(2,2)=3.;

      do i = 1,n 
        do j = 1,n 
          a(i,j) = 1.d0/(i+j-1.d0)
          if(i.eq.j) then
            unity(i,j)= 1.d0
          else
            unity(i,j)=0.d0
          end if
        end do
      end do

!      a(1,1)=6.d0
!      a(1,2)=-2.d0
!      a(1,3)=2.d0
!      a(2,1)=12.d0
!      a(2,2)=-8.d0
!      a(2,3)=6.d0
!      a(3,1)=3.d0
!      a(3,2)=-13.d0
!      a(3,3)=3.d0  

      a_original = a

      write(*,*) 'A'
      do i = 1,n
      write(*,"(5e16.6)") (a_original(i,j), j=1,n)
      end do

!      unity(1,1)=1.d0
!      unity(1,2)=0.d0
!      unity(1,3)=0.d0
!      unity(2,1)=0.d0
!      unity(2,2)=1.d0
!      unity(2,3)=0.d0
!      unity(3,1)=0.d0
!      unity(3,2)=0.d0
!      unity(3,3)=1.d0

  
      call dgesv(n,n,a,n,ipiv,unity,n,info) 

      do j=1,n
      do i=1,n
      a_inverse(i,j)=unity(i,j)
      enddo
      enddo
      
      write(*,*) 'A^-1'
      do i = 1,n
      write(*,"(5e16.6)") (a_inverse(i,j), j=1,n)
      end do

!      unity =0.
!      do i= 1,n
!        do j = 1,n
!          do k= 1,n

!             unity(j,i)=unity(j,i) + a_original(j,k)*a_inverse(k,i)

!          end do 
!          write(*,*)i,j, unity(i,j)
!        end do
!      end do

      unity = 0.d0 
      call dgemm ('n','n',n,n,n,alpha,a_inverse,n,a_original,n,beta, unity,n)
! in c:  dgemm("N","N", &M, &M, &M, &one, A, &M, B, &M, &zero, C, &M);

      write(*,*) 'A^-1 * A'
      do i= 1,n
          write(*,"(5e16.6)") (unity(i,j), j=1,n)
      end do

      unity = 0.d0
      call dgemm ('n','n',n,n,n,alpha,a_original,n,a_inverse,n,beta, unity,n)
! in c:  dgemm("N","N", &M, &M, &M, &one, A, &M, B, &M, &zero, C, &M);

      write(*,*) 'A * A^-1'
      do i= 1,n
          write(*,"(5e16.6)") (unity(i,j),j=1,n)
      end do
 

      stop
      end
